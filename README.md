# Desafio #1

---

## Criação de container Docker

O primeiro passo foi identificar como disponibilizar o arquivo em PDF. 
Pensei primeiro em um link para donwload mas achei uma página estática com Nginx mais apropriado.

Encontrei o projeto do [PDF.JS da Mozilla](https://mozilla.github.io/pdf.js/) de visualização do PDF com quase nenhum código. Pesquisando um pouco mais (e me servindo da vida de infraestrutura que me faz procurar coisas mais simples) encontrei o projeto do [Giuseppeg](https://codesandbox.io/s/pmy914l2kq?file=/index.html) onde me baseei quase completamente.

Depois disso só criei um [dockerfile](Dockerfile) simples com nginx e adicionei os arquivos para a página estática.

Por fim, criei uma imagem e fiz o push pro [Docker Hub](https://hub.docker.com/repository/docker/nekromantheon/nginx-docker).

---

## Criação de infraestrutura GKE

Achei interessante utilizar uma estrutura em cloud para desenvolver o desafio. Assim, encontrei o projeto da Google Cloud Platform, mais especificamente a Google Kubernetes Engine (GKE). 

Fiz o provisionamento de uma conta gratuita e configurei um cluster de servidor com eles. Depois consegui logar e fazer o primeiro deploy com serviço exposto no IP temporário.

A estrutura da GCP é excelente para provisionamento do Kubernetes. A engine realmente facilita a visualização do funcionamento, especialmente quando chegamos na parte de balanceamento de carga, backend e endpoints.

Depois do primeiro deploy aproveitei para copiar o resultado do yaml para a configuração dos contêineres Docker.

```
kubectl describe nginx-1 -o yaml > nginx-2.yaml
```
Com isso consegui configurar o provisionamento da primeira parte do serviço. 

---

## Criação do Service e LoadBalance

Após isso, fiz a configuração do Service para entregar o acesso aos pods. Configurei primeiro um LoadBalancer com a anotação *app: "nginx-2"* que fez o bind direto com o IP temporário do GCP.

Essa configuração é perfeita para testar o deploy. Depois disso, alterei o serviço do Service para NodePort e provisionei o Ingress.

---

## Criação do Ingress e Certificado SSL

Configurei o [Ingress Controller](https://kubernetes.github.io/ingress-nginx/) do Nginx e criei a primeira versão do Ingress provisionando apenas o protocolo HTTP.

O GKE encontra automaticamente essa saída nos balanceadores de carga e já faz o bind com o IP temporário. Consegui enxergar o serviço do PDF entregue em protocolo não seguro.

Depois disso configurei um domínio pelo Google Domain e testei as configurações de DNS para o IP do GKE. Alterei o IP para permanente no GKE e fiz a alteração do DNS apontando para esse IP. Por fim, também configurei o DNS para a liberação do LetsEncrypt com as chaves CAA.

Testei diversas maneiras de adicionar o certificado no Ingress que não envolvessem o LetsEncrypt mas não tive sucesso. Mesmo o processo de criação do Google do certificado não funcionou da maneira desejada. 

Resolvi isso apenas configurando o [Cert-Manager](https://cert-manager.io/docs/) e a autoridade certificadora [ACME](https://cert-manager.io/docs/configuration/acme/) no provider HTTP01.

Alterei as configurações no Ingress para geração do certificado pela LetsEncrypt e consegui gerar o certificado.

O resultado encontra-se [aqui](https://marcokerchner.info/).